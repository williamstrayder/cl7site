import { Container, Link } from "@material-ui/core";
import React from "react";
import PhoneIcon from "@material-ui/icons/Phone";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import logo from "../../imagem/logo-cl7-branco.png";
import useStyles from "./styles";

const Footer = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Container className={classes.container} maxWidth="lg">
        <div className={classes.logo}>
          <img className={classes.logoImg} alt="coop1" src={logo}></img>
        </div>
        <div className={classes.contatos}>
          <div className={classes.containerIcon}>
            <PhoneIcon />
            <span>62 3507-8619</span>
          </div>
          <div className={classes.containerIcon}>
            <WhatsAppIcon />
            <Link
              href="https://api.whatsapp.com/send?phone=5562991137054&text=Em%20que%20posso%20te%20ajudar%3F"
              color="inherit"
              target="_blank"
              rel="noopener"
            >
              <span>62 99113-7054 WhatsApp</span>
            </Link>
          </div>
        </div>
        {/* <div>tres</div> */}
      </Container>
    </div>
  );
};

export default Footer;

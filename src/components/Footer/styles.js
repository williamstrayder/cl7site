import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    // color: theme.palette.primary.contrastText,
    // minHeight: "calc( 100vh)",
    // position: "relative",
    // background: "rgba(255,255,255,1)",
  },
  container: {
    display: "flex",
    maxWidth: "100%",
    padding: 0,
    background: theme.palette.primary.light,
    color: theme.palette.secondary.contrastText,
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
    "& >  div": {
      /* O flex: 1; é necessário para que cada item se expanda ocupando o tamanho máximo do container. */
      flex: 1,
      margin: "0px",
      textAlign: "center",
      fontSize: "1.5em",
    },
  },
  containerIcon: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
    fontSize: "1.3em",
    "& > svg": {
      fontSize: "1.3em",
    },
  },
  logo: {
    padding: "20px",
  },
  logoImg: {
    maxHeight: "150px",
    [theme.breakpoints.down("sm")]: {
      maxHeight: "90px",
    },
  },
  contatos: {
    padding: "20px",
    backgroundColor: theme.palette.primary.main,
  },
}));

export default useStyles;


import React from 'react';
import Sobre from './Sobre/index';
import Galerias from './Galerias';
import Footer from '../../components/Footer';


const Home = () => {
    return(
        <div>
            <Sobre>

            </Sobre>
            <Galerias>

            </Galerias>
            <Footer>

            </Footer>
        </div>
       
    )
}

export default Home;

import BathtubIcon from '@material-ui/icons/Bathtub';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
import HotelIcon from '@material-ui/icons/Hotel';
import useStyles from './styles';


const CardImages = ({ property }) => {
    const {  picture, carSpaces, city, bathrooms, bedrooms ,address} = property;
    const classes = useStyles();

  return (
      <div className={classes.root}>
        <img src={picture} alt="imagem"></img> 
        <div className={classes.descricao}>
          <h1>{city}</h1>
          <div className={classes.endereco}>
            <h2>{address}</h2>
          </div>  
          <div className={classes.icones}>
            <div className={classes.icon}>
                <DirectionsCarIcon/> 
                {carSpaces}
            </div>
            <div className={classes.icon}>
                <BathtubIcon/> 
                {bathrooms}
            </div>
            <div className={classes.icon}>
                <HotelIcon/> 
                {bedrooms}
            </div>
            
            
          </div>
        </div> 
        
      </div>
  );
}
export default CardImages;
